import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {
  public title: string;

  constructor() {
    this.title = 'Variable & script definition'
  }

  ngOnInit() {
  }
}
