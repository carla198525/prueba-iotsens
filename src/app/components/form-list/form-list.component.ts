import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ServiceService } from 'src/app/services/service.service';
import { Evaluation } from 'src/app/models/evaluation';

@Component({
  selector: 'app-form-list',
  templateUrl: './form-list.component.html',
  styleUrls: ['./form-list.component.css']
})
export class FormListComponent implements OnInit {
  public title: string;
  public evaluations: Evaluation[];

  constructor(private service:ServiceService, private router:Router) {
    this.title = 'Script evaluation';
  }

  ngOnInit() {
    this.service.getEvaluateScripts()
    .subscribe(data=>{
      this.evaluations=data;
    })
  }

  evaluateAll() {
    this.service.getEvaluateAllScripts()
    .subscribe(data=>{
      this.evaluations=data;
    })
  }

}
