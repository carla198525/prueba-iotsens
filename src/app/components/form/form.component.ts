import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ServiceService } from 'src/app/services/service.service';
import { Script } from 'src/app/models/script';
import { FormControl, FormGroup, FormBuilder, FormArray } from '@angular/forms';
import { Variable } from 'src/app/models/variable';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  addForm: FormGroup;
  variables: FormArray;

  constructor(private router: Router, private service: ServiceService, private fb: FormBuilder) { }

  ngOnInit() {
    this.addForm = this.fb.group({
      text: [''],
      variables: this.fb.array([ this.createItem() ])
    });
  }

  createItem(): FormGroup {
    return this.fb.group(new Variable());
  }

  guardar(formValue: any) {
    const script = new Script();
    script.text = formValue.text;
    script.variables = formValue.variables;
    
    this.service.createScript(script)
      .subscribe(data => {
        alert('Guardado con éxito!');
        this.router.navigate(["list"]);
      });
  }

  addVariable() {
    this.variables = this.addForm.get('variables') as FormArray;
    this.variables.push(this.createItem());
  }

  removeVariable(index: number) {
    const control = <FormArray>this.addForm.controls['variables'];
    control.removeAt(index);
  }

}
