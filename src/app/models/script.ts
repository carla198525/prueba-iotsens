import { Variable } from '../models/variable';

export class Script {
    id: number;
    variables: Variable[];
    text: string;
}