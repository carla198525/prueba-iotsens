export class Evaluation {
    id: number;
    name: string;
    result: string;
}