import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  public title: string;

  constructor(private router:Router) {
    this.title = 'Variable & script definition';
  }

  List() {
    this.router.navigate(["list"]);
  }

  Add() {
    this.router.navigate(["add"]);
  }
}
