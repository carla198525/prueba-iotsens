import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormListComponent } from './components/form-list/form-list.component';
import { AddComponent } from './components/add/add.component';

const routes: Routes = [
  {path:'list', component:FormListComponent},
  {path:'add', component:AddComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
