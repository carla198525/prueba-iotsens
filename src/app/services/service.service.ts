import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Evaluation } from '../models/evaluation';
import { Script } from '../models/script';
import { Variable } from '../models/variable';

@Injectable({
  providedIn: 'root'
})
export class ServiceService {
  
  constructor(private http:HttpClient) { }

  UrlSave='http://localhost:8080/pruebaIoTsens/scripts/add';
  UrlEval='http://localhost:8080/pruebaIoTsens/scripts/evaluate';
  UrlEvalAll='http://localhost:8080/pruebaIoTsens/scripts/evaluateAll';

  getEvaluateScripts() {
    return this.http.get<Evaluation[]>(this.UrlEval);
  }

  createScript(script: Script) {
    return this.http.post<Script>(this.UrlSave, script);
  }

  getEvaluateAllScripts() {
    return this.http.get<Evaluation[]>(this.UrlEvalAll);
  }

}
